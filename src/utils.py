"""Utilities."""

import unittest

import numpy as np


def vector(*floats):
    """Create 3D double numpy array."""
    return np.array(floats, dtype=np.float)


class GtsamTestCase(unittest.TestCase):
    """Base class with GTSAM assert utils."""

    def gtsamAssertEquals(self, actual, expected, tol=1e-2):
        """Helper function that prints out actual and expected if not equal."""
        equal = actual.equals(expected, tol)
        if not equal:
            raise self.failureException(
                "Values are not equal:\n{}!={}".format(actual, expected))
