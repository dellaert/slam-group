"""Example of SE(2) version of SLAM group non-linear observer from Mahony & Hamel."""

from __future__ import print_function

import time

import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D  # pylint: disable=W0611

import gtsam.utils.plot as gtsam_plot
from group_se2 import GroupSE2, euler_step
from gtsam import Point2, Pose2
from observer_se2 import observer
from raw_se2 import RawSE2, constant_twist_example, h_se2_relative
from utils import vector

# pylint: disable=E0401, E1130


def plot_raw(axes, Xi, spec):
    """Plot raw coordinates on axes, with given point spec."""
    gtsam_plot.plot_pose2_on_axes(axes, Xi._P)
    t = Xi._P.translation()
    axes.plot([t.x()], [t.y()], spec)
    for p in Xi._ps:
        axes.plot([p.x()], [p.y()], spec)


def plot_pi(axes, Xi, spec):
    """Project to egocentric coordinates and plot.."""
    for p in Xi.pi():
        axes.plot([p.x()], [p.y()], spec)


def plot_trajectory(trajectory, fignum=1, spec='g.', raw=False):
    """Plot an entire trajectory."""
    # Set up plotting
    fig1 = plt.figure(fignum)
    axes = fig1.gca()

    plot = plot_raw if raw else plot_pi

    for _t, Xi_t in trajectory:
        plot(axes, Xi_t, spec)

    plt.axis('equal')


def create_ground_truth_trajectory(V, dt, num_steps, num_points=-1):
    """Create trajectory, if num_points<0 use 4 fixed landmarks"""
    if num_points < 0:
        points = [Point2(-3, 3), Point2(3, 2), Point2(4, -2), Point2(-6, 5)]
    else:
        ps = 20.0*np.random.rand(20, 2)
        points = [Point2(p[0], p[1]) for p in ps]
    return constant_twist_example(V, dt=dt, N=num_steps, points=points)


def simulate_measurement(sigma, Xi):
    """Simulate noisy measurements."""
    ys = h_se2_relative(Xi)
    return [y + sigma * np.random.randn(3) for y in ys]


def gob_slam(V, dt, measurements):
    """Run SE(2) observer on measurements."""
    # Set reference configuration from first set of relative measurements
    t0, ys_0 = measurements[0]
    pp = [Point2(y[0], y[1]) for y in ys_0]
    Xi_z = RawSE2(Pose2(), *pp)
    ys0 = h_se2_relative(Xi_z)

    # Set gains
    K = len(ys_0)
    k, l = 0.05/K, 0.01/K

    # Initialize estimate on SLAM group
    zXe = GroupSE2.identity(K)

    # Loop over measurements
    estimated = []
    for t, ys_t in measurements:
        # convert this to measurements as would be observed at Xi_z
        eXz = zXe.inverse()
        ys0_t = eXz.act_on_outputs(ys_t)

        # calculate observer dynamics
        delta = observer(ys0, ys0_t, zXe, k=k, l=l)

        # Add velocity and update estimate
        delta_V = delta.add_body_velocity(V)
        zXe = euler_step(zXe, delta_V, dt)
        estXi = zXe.act_on_raw(Xi_z)
        estimated.append((t, estXi))

    return estimated


def run():
    """Run SE(2) observer on simulated trajectory."""
    K = 20
    dt = 1.0
    V = vector(0.5, 0, 0.05)  # constant body velocity twist
    ground_truth = create_ground_truth_trajectory(V, dt, 50, num_points=K)

    sigma = 0.5 * vector(1, 1, 0)  # measurement noise
    measurements = [(t, simulate_measurement(sigma, Xi_t))
                    for t, Xi_t in ground_truth]

    # Run SLAM
    estimated = gob_slam(V, dt, measurements)

    # plot
    plot_trajectory(ground_truth, 1, 'g.')
    plot_trajectory(estimated, 2, 'r.')
    plot_trajectory(ground_truth, 3, 'g.', True)
    plot_trajectory(estimated, 4, 'r.', True)
    plt.show()


if __name__ == "__main__":
    run()
