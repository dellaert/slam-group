"""Raw SE(2) SLAM coordinates."""

# pylint: disable=E0401, E1130

import unittest

import numpy as np

from gtsam import Point2, Point3, Pose2
from utils import GtsamTestCase, vector


class LandmarksSE2(object):
    """A collection of Point2 landmarks."""

    def __init__(self, *args):
        """Construct from variable argument list."""
        self._ps = args

    def equals(self, xi, tol):
        """Check equality up to a tolerance."""
        for p, q in zip(self._ps, xi._ps):
            if not p.equals(q, tol):
                return False
        return True

    def __len__(self):
        return len(self._ps)

    def __iter__(self):
        return self._ps.__iter__()

    def __getitem__(self, i):
        return self._ps[i]


class RawSE2(object):
    """SE(2) SLAM instance in raw/total space coordinates."""

    def __init__(self, P, *args):
        """Construct from a pose2 and a variable number of landmarks p."""
        assert isinstance(P, Pose2)
        assert isinstance(args[0], Point2)
        self._P = P
        self._ps = LandmarksSE2(*args)

    def __repr__(self):
        s = "[\nP = {}".format(self._P)
        for i, p in enumerate(self._ps):
            s += "p_{} = {}".format(i, p)
        return s+"]"

    def equals(self, other, tol):
        """Check equality up to a tolerance."""
        return self._P.equals(other._P, tol) and self._ps.equals(other._ps, tol)

    def pi(self):
        """ Project onto egocentric SLAM representative.
            These are the coordinates for the SLAM manifold that Rob does not like.
        """
        relative_landmarks = [self._P.transform_to(p) for p in self._ps]
        return LandmarksSE2(*relative_landmarks)


def create_example():
    """Create an example trajectory with 2 poses."""
    P0 = Pose2(2, 1, 0)
    P1 = Pose2(3, 1, 0)
    ps = [Point2(3, 2), Point2(3, 0)]
    return [(0.0, RawSE2(P0, *ps)), (0.0, RawSE2(P1, *ps))]


def constant_twist_example(twist, N=10, dt=1.0, points=[Point2(3, 2), Point2(4, -2)]):
    """Create an example trajectory with 2 poses."""
    return [(dt*i, RawSE2(Pose2.Expmap(i*twist*dt), *points)) for i in range(N)]


def h_se2_relative(Xi):
    """ Measurement frunction, from total space to relative landmark measurements.
        returns list of 3-vectors.
    """
    assert isinstance(Xi, RawSE2)
    point2s = [Xi._P.transform_to(p) for p in Xi._ps]
    return [vector(p.x(), p.y(), 1) for p in point2s]


class TestRawSE2(GtsamTestCase):
    def test_example(self):
        """Text example creation."""
        trajectory = create_example()
        self.assertIsInstance(trajectory, list)
        t0, total0 = trajectory[0]
        self.assertIsInstance(t0, float)
        self.assertIsInstance(total0, RawSE2)

    def test_h_se2_relative(self):
        """Test measurement function."""
        Xi = RawSE2(Pose2(2, 1, 0), Point2(3, 2), Point2(3, 0))
        y = h_se2_relative(Xi)
        self.assertIsInstance(y, list)
        self.assertEquals(len(y), 2)
        np.testing.assert_array_equal(y[0], vector(1, 1, 1))
        np.testing.assert_array_equal(y[1], vector(1, -1, 1))

    def test_pi(self):
        Xi = RawSE2(Pose2(2, 1, 0), Point2(3, 2), Point2(3, 0))
        xi = Xi.pi()
        self.assertIsInstance(xi, LandmarksSE2)
        self.assertEquals(len(xi), 2)
        self.gtsamAssertEquals(xi[0], Point2(3-2, 2-1))
        self.gtsamAssertEquals(xi[1], Point2(3-2, 0-1))


if __name__ == "__main__":
    unittest.main()
