"""Nonlinear observer for SE(2), from Mahony & Hamel."""

# pylint: disable=E0401, E1130

from __future__ import print_function

import unittest

import numpy as np

from group_se2 import GroupSE2, TangentSE2, euler_step
from gtsam import Point2, Point3, Pose2
from raw_se2 import RawSE2, create_example, h_se2_relative
from utils import GtsamTestCase, vector


def project_se2(M):
    """Project 3*3 matrix to se(2) element."""
    M1 = M[:2, :2]
    skew = 0.5*(M1-M1.T)
    return vector(skew[1, 0], M[0, 2],  M[1, 2])


def observer(y0s, y0s_t, zXe, k=1.0, l=1.0):
    """ Calculate observer dynamics, returns element tangent to total space.
        y0s -- measurements for reference configuration (2D homogeneous vectors)
        y0s_t -- measurements at time t, converted back to reference config.
    """
    # Check inputs
    assert len(y0s_t) > 0
    assert len(y0s_t) == len(y0s)
    assert y0s[0].shape == (3,), y0s[0].shape
    assert y0s_t[0].shape == (3,), y0s_t[0].shape

    # Calculate Delta = wedge(D_ref)
    errors = [y0_t-y0 for y0_t, y0 in zip(y0s_t, y0s)]
    M = sum([k * np.outer(e, y0_t) for e, y0_t in zip(errors, y0s_t)])
    D_ref = project_se2(-M)
    Delta = Pose2.wedge(D_ref[0], D_ref[1], D_ref[2])

    # Calculate small ds
    ds = [e*l/k + np.dot(Delta, a) for e, a in zip(errors, zXe._as)]

    # Return tangent vector on total space
    eTz = zXe._A.inverse()
    D_body = eTz.Adjoint(D_ref)
    return TangentSE2(D_body, *ds)


class TestObserver(GtsamTestCase):
    def test_project_se2(self):
        """Test projection of 3*3 matrix to se(2) element."""
        M = np.array([[0, -2, 3], [2, 0, 6], [7, 8, 9]])
        D = project_se2(M)
        self.assertEquals(D.shape, (3,))
        np.testing.assert_array_equal(D, vector(2, 3, 6))

    def test_group_compose(self):
        """Test that SLAM group compose works."""
        X = GroupSE2.identity(2)
        self.assertIsInstance(X.compose(X), GroupSE2)

    def test_observer(self):
        """Test calculation of observer dynamics."""
        Xi0 = create_example()[0][1]
        y0s = h_se2_relative(Xi0)
        zXe = GroupSE2.identity(2)
        eXz = zXe.inverse()
        y0s_t = eXz.act_on_outputs(y0s)
        delta = observer(y0s, y0s_t, zXe)
        self.assertIsInstance(delta, TangentSE2)
        np.testing.assert_array_equal(delta._D, vector(0, 0, 0))
        np.testing.assert_array_equal(delta._ds[0], vector(0, 0, 0))
        np.testing.assert_array_equal(delta._ds[1], vector(0, 0, 0))


if __name__ == "__main__":
    unittest.main()
