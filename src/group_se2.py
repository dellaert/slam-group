"""SLAM group for SE(2)."""

# pylint: disable=E0401, E1130

import unittest

import numpy as np

from gtsam import Point2, Point3, Pose2
from raw_se2 import RawSE2, create_example, h_se2_relative
from utils import GtsamTestCase, vector


class GroupSE2(object):
    """SE(2) SLAM group instance."""

    def __init__(self, A, *args):
        """Construct from a pose2 and a variable number of landmarks deltas."""
        assert isinstance(A, Pose2)
        for a in args:
            assert a.shape == (3,)
            assert a[2] == 0.0
        self._A = A
        self._as = args

    def __repr__(self):
        s = "[\nA = {}".format(self._A)
        for i, a in enumerate(self._as):
            s += "a_{} = {}\n".format(i, a)
        return s+"]"

    @staticmethod
    def identity(N):
        aa = N*[vector(0, 0, 0)]
        return GroupSE2(Pose2(), *aa)

    def equals(self, X, tol):
        """Check equality up to a tolerance."""
        if not self._A.equals(X._A, tol):
            return False
        for a, b in zip(self._as, X._as):
            if not Point3(*a).equals(Point3(*b), tol):
                return False
        return True

    def compose(self, X):
        """Compose with other group element."""
        R = self._A.rotation()

        def compose_ab(a, b):
            p = R.rotate(Point2(b[0], b[1]))
            return vector(a[0]+p.x(), a[1]+p.y(), 0)

        aa = [compose_ab(a, b) for a, b in zip(self._as, X._as)]
        return GroupSE2(self._A.compose(X._A), *aa)

    def inverse(self):
        """Inverse operation."""
        Ainv = self._A.inverse()
        Rinv = Ainv.rotation()

        def invert_a(a):
            p = Rinv.rotate(Point2(a[0], a[1]))
            return vector(-p.x(), -p.y(), 0)

        aa = [invert_a(a) for a in self._as]
        return GroupSE2(Ainv, *aa)

    def act_on_raw(pXq, Xi):
        """ Right group action of pXq on raw SLAM coordinates in inertial frame.
            pXq is interpreted as:
                - a correction pXq._A for the robot pose wTp
                - corrections ai, expressed in robot frame, for landmarks
            Returns Xi * pXq, raw SLAM coordinates updated with pXq.
        """
        assert isinstance(Xi, RawSE2)
        assert len(Xi._ps) == len(pXq._as)
        wTp = Xi._P  # pose of robot in inertial frame
        wRp = wTp.rotation()  # rotation from robot to inertial frame

        def act_on_p(pi, ai):
            wi = wRp.rotate(Point2(ai[0], ai[1]))  # convert to inertial
            return Point2(pi.x() + wi.x(), pi.y() + wi.y())

        pp = [act_on_p(pi, ai) for pi, ai in zip(Xi._ps, pXq._as)]
        return RawSE2(wTp.compose(pXq._A), *pp)

    def act_on_manifold(self, xi):
        """Group action on relative landmark representatives."""
        # self-fulfilling prophecy:
        assert len(xi) == len(self._as)
        Xi = RawSE2(Pose2(), *xi)
        return self.act_on_raw(Xi).pi()

    def act_on_outputs(self, measurements):
        """ Group action of pXq on relative measurements.
            measurements -- outputs y in inertial frame
            returns measurements as would be measured in B frame
        """
        assert len(measurements) == len(self._as)
        aTb = self._A
        bTa = aTb.inverse().matrix()
        return [np.dot(bTa, y_i+a_i) for a_i, y_i in zip(self._as, measurements)]


class TangentSE2(object):
    """Element tangent to SLAM group, se2 * R^2 * R^2 *...."""

    def __init__(self, D, *args):
        """ Construct from a pose se2 element (3-vector) and a variable number of
            landmarks updates d (2D zero-homogeneous vectors).
        """
        assert D.shape == (3,)
        assert args[0].shape == (3,)
        self._D = D
        self._ds = args

    def __repr__(self):
        s = "[\nD = {}\n".format(self._D)
        for i, d in enumerate(self._ds):
            s += "d_{} = {}\n".format(i, d)
        return s+"]"

    def add_body_velocity(self, V):
        """Add body velocity to D component, returns new object."""
        return TangentSE2(self._D+V, *self._ds)


def euler_step(X, delta, dt):
    """Do an Euler step on X with TangentSE2 delta."""
    assert isinstance(X, GroupSE2)
    assert isinstance(delta, TangentSE2)
    assert isinstance(dt, float)
    aa = [a_i + dt * delta_i for a_i, delta_i in zip(X._as, delta._ds)]
    return GroupSE2(X._A.compose(Pose2.Expmap(delta._D)), *aa)


class TestGroupSE2(GtsamTestCase):

    def test_group_inverse(self):
        """Test that SLAM group inverse works."""
        I = GroupSE2.identity(2)
        X = GroupSE2(Pose2(1, 2, 3), vector(3, 4, 0), vector(5, 6, 0))
        self.assertIsInstance(X.inverse(), GroupSE2)
        self.gtsamAssertEquals(X.inverse().compose(X), I)
        self.gtsamAssertEquals(X.compose(X.inverse()), I)

    def test_group_action(self):
        """Test group action property satisfied."""
        X = GroupSE2(Pose2(1, 2, 3), vector(3, 4, 0), vector(5, 6, 0))
        Xi = create_example()[0][1]
        Xi0 = X.act_on_raw(X.act_on_raw(Xi))
        Xi1 = X.compose(X).act_on_raw(Xi)
        self.gtsamAssertEquals(Xi1, Xi0)

    def test_act_on_manifold(self):
        """Test property (37) in journal paper."""
        Xi = create_example()[0][1]
        X = GroupSE2(Pose2(1, 2, 3), vector(3, 4, 0), vector(5, 6, 0))
        rhs = X.act_on_manifold(Xi.pi())
        lhs = X.act_on_raw(Xi).pi()
        self.gtsamAssertEquals(rhs, lhs)

    def test_act_on_outputs(self):
        """Test that SLAM group acts on outputs in the correct way."""
        Xi = create_example()[0][1]
        measurements = h_se2_relative(Xi)
        pXq = GroupSE2(Pose2(1, 2, 3), vector(3, 4, 0), vector(5, 6, 0))
        Xi_new = pXq.act_on_raw(Xi)
        new_measurements = h_se2_relative(Xi_new)
        for y0, y1 in zip(pXq.act_on_outputs(measurements), new_measurements):
            np.testing.assert_array_almost_equal(y0, y1)

    def test_euler_step(self):
        X = GroupSE2.identity(2)
        twist = vector(1, 0, 0)
        ds = [vector(0.1, 0, 0), vector(0.1, 0, 0)]
        delta = TangentSE2(twist, *ds)
        dt = 0.5
        Y = euler_step(X, delta, dt)
        aa = 2 * [vector(0.1*dt, 0, 0)]
        expected = GroupSE2(Pose2.Expmap(twist), *aa)
        self.gtsamAssertEquals(Y, expected)


if __name__ == "__main__":
    unittest.main()
