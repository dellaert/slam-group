% Fixit script. 
% This is a script to fix up any issues in the data script provided by
% Mina. 

% close all 
% clear all 
% load ex_mina


% add a fourth row of ones to gSLAM_data.Y
for k=1:length(gSLAM_data) 
    gSLAM_data(k).Y(4,:) = ones(1,length(gSLAM_data(k).Y(1,:))) ;      
end

% Data was recorded at 6Hz. 
% rescale the time access so the car is moving at a sensible speed
scale_time = 1/6 ; % 20 Hz data 
for k=1:length(gSLAM_data) 
    gSLAM_data(k).time = scale_time* gSLAM_data(k).time
end

% recompute the velocities to match the data 
for k=2:length(gSLAM_data) 
    V_temp = (1/gSLAM_data(k-1).time)*logm(inv(gSLAM_true(k-1).P)*gSLAM_true(k).P)
    gSLAM_data(k).robotvel_lin_bff = V_temp(1:3,4) ; 
    gSLAM_data(k).robotvel_ang_bff(1) = V_temp(3,2) ; 
    gSLAM_data(k).robotvel_ang_bff(2) = V_temp(1,3) ; 
    gSLAM_data(k).robotvel_ang_bff(3) = V_temp(2,3) ; 
end    

% set a velocity for robotvel for k =1
gSLAM_data(1).robotvel_lin_bff = gSLAM_data(2).robotvel_lin_bff ; 
gSLAM_data(1).robotvel_ang_bff = gSLAM_data(2).robotvel_ang_bff ; 

% % add a fourth row of ones to gSLAM_data.p
% for k=1:length(gSLAM_true) 
%     gSLAM_true(k).p(4,:) = ones(1,length(gSLAM_true(k).p(1,:))) ;      
% end

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%% Reconstruct arrays for plotting 

N = length(gSLAM_data) ; 

for k= 1:N 
% reconstruct position of true trajectory 
x_P(:,k) = [gSLAM_true(k).P(1,4);  gSLAM_true(k).P(2,4) ; gSLAM_true(k).P(3,4)] ; 
end

for k= 1:N
% reconstruct measurements in the body fixed frame
y1_bff(:,k) = gSLAM_data(k).Y(:,1) ; 
y1_ref(:,k) = gSLAM_true(k).P * gSLAM_data(k).Y(:,1) ; 
end

for k= 1:N
% reconstruct measurements in the body fixed frame
robovel_lin(:,k) = gSLAM_data(k).robotvel_lin_bff(:) ; 
robovel_ang(:,k) = gSLAM_data(k).robotvel_ang_bff(:) ; 
end

%
% Display
close all 

figure(1)
title('True data')
hold on
% plot the trajectory of the lifted kinematics  
for k= 1:N 
plot3(x_P(1,:),x_P(2,:),x_P(3,:),'-b')
    for j = 1:length(gSLAM_true(k).p)
%         plot3(gSLAM_true(k).p(1,j),gSLAM_true(k).p(2,j),gSLAM_true(k).p(3,j),'*b')
    end
plot3(y1_ref(1,k), y1_ref(2,k), y1_ref(3,k),'*r') 
end

figure(2)
title('True velocities')
subplot(2,1,1) 
hold on
plot(robovel_lin(1,:))
plot(robovel_lin(2,:))
plot(robovel_lin(3,:))
subplot(2,1,2)
hold on
plot(robovel_ang(1,:))
plot(robovel_ang(2,:))
plot(robovel_ang(3,:))

return


%%
% cleanup the workspace and return
clearvars -except gSLAM_data gSLAM_true
disp('  gSLAM_true and gSLAM_data are generated ...  good luck') 
return

    