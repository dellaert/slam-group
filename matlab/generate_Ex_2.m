% This code generates a data file for the gSLAM code 
% the file should be replaced by real data. 
% Many of the choices of number of points, number of features, etc, are
% hard coded in this file.  This is OK since this file is a throwaway. 

%% setup 
disp(' Ill start by making up some data here ... ')

%Define the key data files as global 
global gSLAM_data gSLAM_filter gSLAM_true

% number of data instances 
% N = 1000 ; 
N = 1000 ; 
% number of features 
n = 5 ;
% noise in measurements 
sigma_Y = 0.1 ; 

% noise in velocity  
sigma_v_bff = 0.05 ; 
sigma_omega_bff = 0.05 ; 

% sample time offset - we assume constant. 
delta_t = 0.05 ;



%% Local variables 
% All initial states are defined wrt to the nominal reference frame 
% (located at the orgin) 

% Initial state of the robot 
x_P_init = [0 ; -3; 5] ; % put the robot up in the air and offset so that it goes around a circle. 
R_P_init = eye(3) ; 
P_init = [R_P_init x_P_init; zeros(1,3) 1]; 

% Initial state of the environment 
p1_init=[4 0 0 1]';
p2_init=[-4 0 0 1]';
p3_init=[0 4 0 1]';
p4_init=[0 -4 0 1]';
p5_init = [8 -8 0 1]';

% Assume environment static. 
p1_true=[4 0 0 1]';
p2_true=[-4 0 0 1]';
p3_true=[0 4 0 1]';
p4_true=[0 -4 0 1]';

% moving piont in the environment 
% define its velocity 
p5_dot = [-0.3 +0.3 0 0]';

% Robot velocity - 
% we assume the robot has consant BFF velocity 
omega_bff = [0; 0; 0.5] ; 
v_bff = [1.5; 0; 0 ] ; 

%% trajectory and measured data 

% set the initial condition for pose 
gSLAM_true(1).P = P_init ;
% set initial time  
gSLAM_true(1).t = 0 ; 

% generate and store the robot trajectory 
P_k_prev = P_init ; % init 
P_k = zeros(4,4) ; % define array 
for k = 2:N
    % add velocity noise 
    robotV = [Skew_matrix(omega_bff + sigma_omega_bff*randn(3,1)) (v_bff + sigma_v_bff*randn(3,1)) ; zeros(1,4)] ; 
    P_k = P_k_prev*expm(delta_t*robotV) ;
    gSLAM_true(k).P = P_k ;     
    P_k_prev = P_k ; % reset
end

% specify the true point array 
p_k = [p1_init p2_init p3_init p4_init] ; % this is always the same
gSLAM_true(1).p = p_k ;   
gSLAM_true(1).p(:,5) = p5_init ; 
gSLAM_true(1).p_vel = [zeros(4,4) p5_dot] ; 
for k = 2:N
    gSLAM_true(k).p = p_k ; 
    gSLAM_true(k).p(:,5) = gSLAM_true(k-1).p(:,5) + delta_t*p5_dot ; 
    gSLAM_true(k).p_index = [1 2 3 4 5] ; 
    gSLAM_true(k).p_vel = [zeros(4,4) p5_dot] ; 
end

% specify the data file
gSLAM_data(1).time = 0 ; 
gSLAM_data(1).robotvel_lin_bff = v_bff ;
gSLAM_data(1).robotvel_ang_bff = omega_bff ; 
gSLAM_data(1).Yindex = [1 2 3 4 5] ; % all points always visible in same order
% measure the observed point with noise 
gSLAM_data(1).Y = inv(P_init)*gSLAM_true(1).p + [sigma_Y.*rand(3,1); 0] ; 

for k = 2:N
    gSLAM_data(k).time = gSLAM_data(k-1).time + delta_t ; 
    gSLAM_data(k).robotvel_lin_bff = v_bff ;
    gSLAM_data(k).robotvel_ang_bff = omega_bff ; 
    gSLAM_data(k).Yindex = [1 2 3 4 5] ; % all points always visible in same order
    % measure the points with added noise
    gSLAM_data(k).Y = inv(gSLAM_true(k).P) * gSLAM_true(k).p + [sigma_Y.*randn(3,1); 0] ; 
end

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%% Reconstruct arrays for plotting 

for k= 1:N 
% reconstruct position of true trajectory 
x_P(:,k) = [gSLAM_true(k).P(1,4);  gSLAM_true(k).P(2,4) ; gSLAM_true(k).P(3,4)] ; 
end

for k= 1:N
% reconstruct true points - note they may be time varying 
p(:,:,k) = gSLAM_true(k).p(:,:) ; 
end

for k= 1:N
% reconstruct measurements in the body fixed frame
y1_bff(:,k) = gSLAM_data(k).Y(:,1) ; 
y1_ref(:,k) = gSLAM_true(k).P * gSLAM_data(k).Y(:,1) ; 
end


% Display

figure(1)
title('True data')
hold on
% plot the trajectory of the lifted kinematics  
for k= 1:N 
plot3(x_P(1,:),x_P(2,:),x_P(3,:),'-b')
    for j = 1:length(p(1,:,1))
        plot3(p(1,j,k),p(2,j,k),p(3,j,k),'*b')
    end
plot3(y1_ref(1,k), y1_ref(2,k), y1_ref(3,k),'*r') 
end

% figure(2)
% title('measured data')
% hold on
% for k= 1:N 
% plot3(y1_bff(1,:),y1_bff(2,:),y1_bff(3,:),'-r')
% end

%%
% cleanup the workspace and return
clearvars -except gSLAM_data gSLAM_true
disp('  gSLAM_true and gSLAM_data are generated ...  good luck') 
return

