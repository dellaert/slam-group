Measurement data structure.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Measured data
Variable name: gSLAM_data
Data Instances: gSLAM_data(k)

structures:
The data is indexed by an integer k = 1, ...
Each index has a corresponding set of measurements.

variable: gSLAM_data(k).time
notes: time stamp at which data instance k was taken.
* time values should be in ascending order.

name: gSLAM_data(k).robotvel_lin_bff
notes: Robot linear velocity (in body-fixed frame)

name: gSLAM_data(k).robotvel_ang_bff
notes: Robot angular velocity (in body-fixed frame)

name: gSLAM_data(k).Yindex
note:  For each time k, a certain subset of features are measured.
The Yindex array lists the reference index for the features observed.
The i'th index denotes the index of the feature point measurement
that is stored in the i'th column of gSLAM_data(k).Y

name: gSLAM_data(k).Y
notes: These are the point measurements seen at time k.
This is a 4 x length(Yindex) array.
the i'th column corresponds to the point feature with index
gSLAM_data(k).Yindex(i) written in homogeneous coordiantes (4th element equals 1).

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Ground truth data
Variable name: gSLAM_true
Data Instances: gSLAM_true(k)

label:gSLAM_true(k).P
note: Robot Pose with respect to the world reference.

label:
note:

label:
note:

label:
note:

label:
note:

label:
note:

label:
note:

label:
note:

label:
note:

label:
note:



~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Filter data
Structure: gSLAM_filter
Data Instances: gSLAM_filter(k)

label: gSLAM_filter(k).A_hat
note:

label: gSLAM_filter(k).a_hat
note:

label: 
note:

label:
note:

label:
note:

label:
note:

label:
note:

label:
note:














z
