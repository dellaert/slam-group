% gobSLAM - Geometric Observer Simultaneous Localisatin and Mapping. 
% This code is developed for SLAM applications 

% Copyright 2017 The Australian National University
% Author: Robert MAHONY and Tarek HAMEL
% Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
% 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Notes

% debug ------- 
% sections to be removed. 
% debug ------- 

%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%% Initialisation
disp('OK - here we go again, did you chose good gains? Too late to change them now ... ') 

% check below here for the user settings. 

% Clear workspace 
clear all
close all 

disp(' Ill call the subroutine to prepare the data ... ') 
disp(' I think today we will be using generate_Ex_5 ... ') 
% Load the experiment 
generate_Ex_5
% load ex_mina
% Fixit_script

%Set the data files to global.  This is probably not necessary 
global gSLAM_data gSLAM_filter gSLAM_true

% set a variable to encode the number of data instances 
N_exp = length(gSLAM_data) ;

% compute the number of features in total. 
number_features = 0 ;
for k=1:N_exp 
    number_features = max([gSLAM_data(k).Yindex number_features]) ;
end

%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% User setting 

% Define the control gains 
gain_l = 0.5/number_features; 
% this gain setting is then divided by the number of data points to
% normalise. 
gain_k = 0.5/number_features ; 
gain_m = 5; % use for not noisey data - in order to see the transient better. 
% gain_m = 1; % use for noisey data. 


% Sometimes you may want to run the algorithms using the true intial
% conditions to test what it does. 
% set flag to 1 to use true initial conditions. 
% set to zero to use other conditions. 
UseTrueIC_flag = 0; 

%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%% SLAM algorithm initialisation 

disp('   Initialise the SLAM problem') 

% set a variable to encode the number of data instances 
N_exp = length(gSLAM_data) ;

% initialise the filter structure by setting a time variable.  
%- 
% initialize time 
gSLAM_filter(1).time = gSLAM_data(1).time ; 
% set the initial condition for pose to identity 
% check if UseTrueIC_flag is set.  if so use true IC. 
% otherwise set to identity
if [UseTrueIC_flag == 1] 
    P_0 = gSLAM_true(1).P ; 
    disp(' defin P_0 from true data ')
else
    P_0 = eye(4) ; % unless there is some prior information. 
    P_0 = P_0 + [zeros(4,3) [-1;0;2;0] ] ;  
end

% set the initial environment points to the first observation
% check if the UseTreuIC_flag is set and if so use the given p_0 
% otherwise, set the p_0 from the first measurement. 
if [UseTrueIC_flag == 1] 
    p_0 = gSLAM_true(1).p ; 
    disp(' defin p_0 from true data ')
else
    for j = 1:length(gSLAM_data(1).Yindex) % index through the observations
        % Take the j'th column of Y.
        % put it in the Yindex(j) column of p_hat.  
        p_0(:,gSLAM_data(1).Yindex(j)) = P_0 * gSLAM_data(1).Y(:,j) ;
    end
end

% set the initial group SE(3) element to identity 
gSLAM_filter(1).A_hat = eye(4) ; 
% set the initial group R^3 elements to zero 
% these are set to zero since the p_0 is set to the observation
for j = 1:length(gSLAM_data(1).Yindex) % index through the observations
% Set the Yindex(j) column of a_hat to zero 
gSLAM_filter(1).a_hat(:,gSLAM_data(1).Yindex(j)) = [zeros(3,1); 0 ] ; 
end
% set the env motion state initial conditions.
if [UseTrueIC_flag == 1]
    for j = 1:length(gSLAM_data(1).Yindex) % index through the observations
        % Set all the w_hat velocities based on known p_vel data. 
        % note that we need to transform the inertial env. vel into the BFF
        % and then apply A_hat to generate the true w. 
        gSLAM_filter(1).w_hat = gSLAM_filter(1).A_hat * inv(gSLAM_true(1).P)*gSLAM_true(1).p_vel ; 
        dips(' for true data, set the w_hat velocities truly... ')
    end
else 
    % set the velocities to zero since this is the best estimate we have. 
    for j = 1:length(gSLAM_data(1).Yindex) % index through the observations
        % Set the Yindex(j) column of w_hat to zero 
        gSLAM_filter(1).w_hat(:,gSLAM_data(1).Yindex(j)) = zeros(4,1) ; 
    end
end

% Initialise the Lyapunov function 
% The contributions due to features are all zero since we initialise the
% features with the initial measurements. 
temp_Lyap = 0 ; 
%
% The velocity errors are not zero 
% compute the velocity error signal as an array. 
A_temp = gSLAM_filter(1).w_hat(:,1:number_features) ; 
B_temp = gSLAM_true.p_vel ; 
w_tilde = A_temp - B_temp  ;
    norm_w_tilde = sum(diag(w_tilde*w_tilde')) ; 
gSLAM_filter(1).Lyap = temp_Lyap + norm_w_tilde; 

%%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%% SLAM algorithm  
disp('    Compute the filter - yippeee ... ')
% there is a single outer loop iteration for the data instances k 
% initialization has set the k=1 case.  Start from k=2
% each step of the algorithm computes filter estimates at time k based on
% previous filter state at k-1 and new data at time k. 

for k = 2:N_exp
% the algorithm is structured internally into three parts. 
%- 
% part I: This part computes a velocity update for all states, including
% the newly defined. 
%- 
% part II: this checks to see if new points are observed and initialises
% new states. 
%- 
% part III: This part computes the innovation update.  

% -------------------------------------------------- 
% preliminaries 
% set time 
gSLAM_filter(k).time = gSLAM_data(k).time ; 

%-------------------------------------------------------------------------
% part I: This part computes a velocity update for all existing states 

% define the time step 
delta_time = gSLAM_data(k).time - gSLAM_data(k-1).time ; 

% build the robot velocity matrix 
V_wedge = [Skew_matrix(gSLAM_data(k-1).robotvel_ang_bff) gSLAM_data(k-1).robotvel_lin_bff ; zeros(1,4) ] ; 
% update the robot pose 
gSLAM_filter(k).A_hat = gSLAM_filter(k-1).A_hat * expm(delta_time*V_wedge) ;  
% update a_hat estimates 
gSLAM_filter(k).a_hat = gSLAM_filter(k-1).a_hat + delta_time * gSLAM_filter(k-1).w_hat ; 
% update w_hat estimates 
% The constant velocity assumption means that this update is trivial. 
gSLAM_filter(k).w_hat = gSLAM_filter(k-1).w_hat ; 

%-------------------------------------------------------------------------
% part II: checks to see if new points are observed and initialises new states. 

% compute an estimate of P_hat based on the forward estimate. 
P_hat_k = P_0 * gSLAM_filter(k).A_hat ; 

for j = 1:length(gSLAM_data(k).Yindex) % index through the observations
    % set a variable for picking new features. 
    temp_number_features = length(p_0(1,:)) ; % counts the number of existing features. 
    % if Yindex value is larger than existing feature then there is a new features. 
    if [gSLAM_data(k).Yindex(j) > temp_number_features ] 
        % Check if the UseTrueIC_flag is set.  
        % If not - set the p_0 value by transforming the measurement into 
        % inertial frame using the latest estimate of the state. 
        if [UseTrueIC_flag == 1]
           % nothing needs to be done here since p_0 was fully set earlier.
        else
            p_0(:,gSLAM_data(k).Yindex(j)) =  P_hat_k * gSLAM_data(k).Y(:,j) ;
        end
        
        % update the Yindex(j) column of a_hat at time k to be zero. 
        % This corresponds to choosing the p_0 column as should be
        % correct at time k.  
        gSLAM_filter(k).a_hat(:,gSLAM_data(k).Yindex(j)) = zeros(4,1) ;
        % Check if UseTrueIC_flag is set.  
        % If not set the initial corresponding velocity to zero. 
        % note that we need to initialise the previous w_hat(k-1) velocity
        % to have the state ready for the update.  
        if [UseTrueIC_flag == 1]
           % nothing needs to be done here since w_hat was fully set earlier.
        else
            gSLAM_filter(k).w_hat(:,gSLAM_data(k).Yindex(j)) = zeros(4,1) ; 
        end
    end 
end

%-------------------------------------------------------------------------
% part III: updates the innovation

% define an array for selecting the feature relevant parts of states 
Yindex = gSLAM_data(k).Yindex(:) ; % just to make the equations look simpler 

% compute the error e variable.  Use an array structure to store all the
% elements. 
% 
% e is given by the following equation 
% e = rho(X_hat^{-1},[y]) = rho( (A_hat^{-1}, -A_hat^{-1} a_hat, [y]) 
%   = [A_hat (y - A_hat^{-1} a_hat] = A_hat y - a_hat 
% 
% The array of Y is indexed by Yindex so I draw out just the columns of
% gSLAM_filter(k).a_hat that are relevant. 
e = gSLAM_filter(k).A_hat * gSLAM_data(k).Y - gSLAM_filter(k).a_hat(:,Yindex) ;

% compute Delta - the innvoation for the SE(3) group 
% Delta = - proj ( sum k_j (e_j - pj_0) e_j' ) 
% define a temp variable to compute the sum 
temp_inn_1 = zeros(4,4) ; 
% iterate through the visible features and add to temp_inn_1 for each new
% feature. 
for j = 1:length(Yindex) 
    temp_inn_1 = temp_inn_1 + gain_k.*(e(:,j) - inv(P_0) * p_0(:,Yindex(j)))*e(:,j)';         
end
% apply the se(3) projection to temp_inn_1 
Delta_k = -[0.5*(temp_inn_1(1:3,1:3) - temp_inn_1(1:3,1:3)')  temp_inn_1(1:3,4) ; zeros(1,4) ]  ;

% compute delta the group position innovation. 
% we compute this as a 4 x length(Y_index) array where each column corresponds 
% to the a_hat vector corresponding to 
% note that this needs to be computed based on the k state as it should use
% the propagated information a_hat(k) has been forwarded integrated for the
% velocity estimate w_hat(k) 

% The fourth element of the column will be zero since the innovation is a
% free homogeneous vector. 
delta_k = (gain_l/gain_k) * (e - inv(P_0) * p_0(:,Yindex)) + Delta_k * gSLAM_filter(k).a_hat(:,Yindex) ; 

% compute w_hat_dot as an array 
w_hat_dot_k = Delta_k * gSLAM_filter(k).w_hat(:,Yindex) + gain_m * gain_k * (e - inv(P_0) * p_0(:,Yindex)) ; 

% group elements update 
% take the existing forward velocity estimates and redefine with a further
% update for the innovation. 
gSLAM_filter(k).A_hat = expm(delta_time * Delta_k) * gSLAM_filter(k).A_hat  ;
% update a_hat estimates 
gSLAM_filter(k).a_hat(:,Yindex) = gSLAM_filter(k).a_hat(:,Yindex) + delta_time * delta_k ;
% update w_hat estimates 
gSLAM_filter(k).w_hat(:,Yindex) = gSLAM_filter(k).w_hat(:,Yindex) + delta_time * w_hat_dot_k ;

% Compute the Lyapunov function value 
% define a temp variable to compute the sum 
temp_Lyap = 0 ; 
% iterate through the visible features and add to temp_inn_1 for each new
% feature. 
    for j = 1:length(Yindex) 
        temp_lyap = temp_Lyap + norm( e(:,j)' - inv(P_0) * p_0(:,Yindex(j)) ) ;
    end
    % add the sum of the velocity errors 
    % compute the velocity error signal as an array. 
    % First you need to define arrays, and then you can use matlab array
    % subtraction. 
    A_temp = gSLAM_filter(k).w_hat(:,1:number_features) ; 
    B_temp = gSLAM_true.p_vel ; 
    w_tilde = A_temp - B_temp  ;
    norm_w_tilde = sum(diag(w_tilde*w_tilde')) ; 
gSLAM_filter(k).Lyap = temp_Lyap + norm_w_tilde; 
end

disp('     All done running the algorithm, but I still have to generate data to display ... ') 

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%% Reconstruct data for plotting 

% If ground truth is available then we want to display the data with
% respect to the ground truth. 
% I will call this correction term S_newref and use the alpha group action
% to change the reference frame of the filter measurements. 

% Note that this is different from having used ground truth to set all the
% initial conditions.  We may have groundtruth available to evaluate the
% algorithms performance without requiring it availble for initial
% conditions. 

% compute the S_newref transform
% we actually compute the inverse because this is what is used in teh
% formluae. 
if [gSLAM_true(1).p(4) == 1]
    % if ground truth is available then at the final time then 
    % P_groundtruth = alpha(S_newref, P_hat)  = alpha(S_newref, P_0 * A_hat) 
    %               = S_newref^{-1} * P_0 * A_hat
    inv_S_newref = gSLAM_true(N_exp).P * inv(P_0 * gSLAM_filter(N_exp).A_hat) ;
else 
    % otherwise just set S_newref to the identity. 
    inv_S_newref = eye(4) ;
end


for k= 1:N_exp
    % compute the corrected P_hat based on filter and S_newref
    P_hat = inv_S_newref * P_0 * gSLAM_filter(k).A_hat ; 
    % reconstruct position of true
    x_P_hat(:,k) = [P_hat(1,4);  P_hat(2,4) ; P_hat(3,4)] ; 
    % reconstruct point estimates implements eq (16)
    % note I have to pad the a_hat matrix to match p_0 for early k values
    p(:,:,k) = inv_S_newref*(p_0 + P_0 * [gSLAM_filter(k).a_hat zeros(4,(length(p_0) - length(gSLAM_filter(k).a_hat)))]) ; 
    gSLAM_filter(k).p = inv_S_newref*(p_0 + P_0 * [gSLAM_filter(k).a_hat zeros(4,(length(p_0) - length(gSLAM_filter(k).a_hat)))]) ; 
    % reconstruct w_hat array
    for j = 1:length(gSLAM_filter(k).w_hat) 
        w_norm(j,k) = norm(gSLAM_filter(k).w_hat(:,j)) ; 
    end
    % define a time scale 
    T(k) = gSLAM_filter(k).time ;
end

% compute the x_P for pose 
for k= 1:N_exp 
    % reconstruct position of true trajectory 
    x_P(:,k) = [gSLAM_true(k).P(1,4);  gSLAM_true(k).P(2,4) ; gSLAM_true(k).P(3,4)] ; 
end

disp('      OK thats done - now it is up to MATLAB to plot things ... ') 

%% Display

% 
figure(3)
hold on
title('Evolution of the velocity estimates of the environment')
for j = 1:length(p_0(1,:))
    plot(T,w_norm(j,:))
end

% 
figure(4)
title('State evolution.  Blue plots reconstructed pose.  Green plots estimates of environment. Red plots true environment')
hold on
% plot the trajectory of the lifted kinematics  
plot3(x_P_hat(1,:),x_P_hat(2,:),x_P_hat(3,:),'-b')
% plot the robot 'true' trajecotry 
plot3(x_P(1,:),x_P(2,:),x_P(3,:),'-k')
% plot the final position fo the robot estimate
plot3(x_P_hat(1,N_exp),x_P_hat(2,N_exp),x_P_hat(3,N_exp),'*b')
% plot the final position fo the true robot state. 
plot3(x_P(1,N_exp),x_P(2,N_exp),x_P(3,N_exp),'ok')
% 
% plot the observer feature estimates and the true feature points 
for k= 1:N_exp
    for j = 1:length(p(1,:,1))
        plot3(p(1,j,k),p(2,j,k),p(3,j,k),'.g')
% sort this out bobat
        %     plot3([gSLAM_filter(k-1).p(1,j) gSLAM_filter(k).p(1,j)],[gSLAM_filter(k-1).p(2,j) gSLAM_filter(k).p(2,j)],[gSLAM_filter(k-1).p(3,j) gSLAM_filter(k).p(3,j)],'g')
    end
    for j = 1:length(gSLAM_true(N_exp).p)
    % plot the true ponts 
    plot3(gSLAM_true(k).p(1,j),gSLAM_true(k).p(2,j),gSLAM_true(k).p(3,j),'.k')
    end
end
% plot the final estimates of feature points. 
for j = 1:length(gSLAM_true(N_exp).p)
    % plot the feature estimates  
    plot3(p(1,j,N_exp),p(2,j,N_exp),p(3,j,N_exp),'*g')
    % plot the true ponts 
    plot3(gSLAM_true(N_exp).p(1,j),gSLAM_true(N_exp).p(2,j),gSLAM_true(N_exp).p(3,j),'ok')
end
% set a good viewing angle. 
view([2,1,1.5])
grid

figure(5) 
title('Evolution of the Lyapunov function')
for k = 2:N_exp 
    plot([T(k-1) T(k)],[gSLAM_filter(k-1).Lyap gSLAM_filter(k).Lyap],'b') 
    hold on 
end

% All done 
disp('       All done now - I hope you enjoyed the experience. ') 


%% RETURN test
return 


% deprecated code
% 
figure(5)
% title('Final states')
hold on % keep the hold on. 
% plot the final position fo the robot estimate
plot3(x_P_hat(1,N_exp),x_P_hat(2,N_exp),x_P_hat(3,N_exp),'*b')
% plot the final position fo the true robot state. 
plot3(x_P(1,N_exp),x_P(2,N_exp),x_P(3,N_exp),'or')
% plot feature points 
for j = 1:length(gSLAM_true(N_exp).p)
    % plot the feature estimates  
    plot3(p(1,j,N_exp),p(2,j,N_exp),p(3,j,N_exp),'*g')
    % plot the true ponts 
    plot3(gSLAM_true(N_exp).p(1,j),gSLAM_true(N_exp).p(2,j),gSLAM_true(N_exp).p(3,j),'ok')
    end
view([2,1,1.5])
grid    

